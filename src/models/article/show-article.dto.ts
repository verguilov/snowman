import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ShowArticleDTO {
    @Expose()
    public title: string;

    @Expose()
    public content: string;
}