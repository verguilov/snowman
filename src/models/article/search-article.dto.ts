import { Expose, Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { Like } from 'typeorm';

export class SearchArticleDTO {
    @Expose()
    @IsOptional()
    @Transform((value) => Like(`%${value || ''}%`))
    public title?: string;
}