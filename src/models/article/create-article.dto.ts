import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class CreateArticleDTO {
    @Expose()
    public title: string;

    @Expose()
    public subTitle: string;

    @Expose()
    public content: string;

    @Expose()
    public author: string;

    @Expose()
    public url: string;
}