import { IsString, Length, IsNotEmpty, IsEmail, IsOptional } from 'class-validator';

export class UserLoginDTO {
  @IsEmail()
  @IsOptional()
  email?: string;

  @IsString()
  @IsOptional()
  @Length(3, 15)
  username?: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  password: string;
}
