import { UserLoginDTO } from '../models/user/user-login.dto';
import { AuthService } from './auth.service';
import { Controller, Post, Body, ValidationPipe } from '@nestjs/common';

@Controller('api/v1/session')
export class AuthController {
  constructor(
    private readonly authenticationService: AuthService,
  ) { }

  @Post('login')
  async login(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UserLoginDTO,
  ): Promise<{ authToken: string }> {
    return await this.authenticationService.login(user);
  }

  @Post('reg')
  async reg(
      @Body() user: UserLoginDTO,
  ): Promise<string> {
      return await this.authenticationService.reg(user);
  }
}
