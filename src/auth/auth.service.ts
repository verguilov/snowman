import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { InvalidUserCredentialsError } from 'src/common/errors/invalid-user-credentials.error';
import { User } from 'src/data/entities/user.entity';
import { UserLoginDTO } from 'src/models/user/user-login.dto';
import { Repository } from 'typeorm';
import { JwtPayload } from './jwt-payload';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly jwtService: JwtService,
      ) { }

      public async login(user: UserLoginDTO): Promise<{ authToken: string }> {
        if (!user.username && !user.email) {
          throw new InvalidUserCredentialsError();
        }
    
        const loginMethod = user.email
          ? { email: user.email }
          : { username: user.username };
    
        const foundUser: User = await this.usersRepository
          .findOne({ where: { ...loginMethod, isDeleted: false } });
    
        if (!foundUser || !(await bcrypt.compare(user.password, foundUser.password))) {
          throw new InvalidUserCredentialsError();
        }
    
        const payload: JwtPayload = {
          id: foundUser.id,
          username: foundUser.username,
          email: foundUser.email,
        };
    
        const authToken: string = await this.jwtService.signAsync(payload);

        console.log(authToken)
    
        return { authToken };
      }

      public async reg(user: UserLoginDTO): Promise<string> {

        if (!user.username && !user.email) {
            throw new InvalidUserCredentialsError();
          }
      
          const loginMethod = user.email
            ? { email: user.email }
            : { username: user.username };
      
          const foundUser: User = await this.usersRepository
            .findOne({ where: { ...loginMethod, isDeleted: false } });

          if(foundUser) {
              throw new Error('User already exists');
          };

          const newUser = this.usersRepository.create();

          newUser.username = user.username;
          newUser.password = await bcrypt.hash(user.password, 10);
          newUser.email = user.email;

          await this.usersRepository.save(newUser);

          return 'gg'
      }
}
