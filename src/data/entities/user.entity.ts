import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('user')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'nvarchar', nullable: false, length: 255 })
    username: string;

    @Column({ type: 'nvarchar', nullable: false, length: 255 })
    email: string;

    @Column({ type: 'nvarchar', nullable: false })
    password: string;
}