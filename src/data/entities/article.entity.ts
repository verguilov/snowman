import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Exclude } from 'class-transformer';

@Entity('article')
export class Article {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'nvarchar', nullable: false, default: 'default title', length: 255 })
    public title: string;

    @Column({ type: 'nvarchar', nullable: false, default: 'default subtitle', length: 400 })
    public subTitle: string;

    @Column({ type: 'longtext', nullable: false })
    public content: string;

    @Column({ type: 'datetime', default: '01.01.2000' })
    public timestamp: Date;

    @Column({ type: 'nvarchar', length: 256 })
    public url: string;

    @Column({ type: 'nvarchar', default: 'the SNOWman'})
    public author: string;

    @Column({ type: 'tinyint', default: false })
    @Exclude()
    public isDeleted: boolean;
}