import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Article } from 'src/data/entities/article.entity';
import { CreateArticleDTO } from 'src/models/article/create-article.dto';
import { Like, Repository } from 'typeorm';

@Injectable()
export class ArticlesService {
    public constructor(
        @InjectRepository(Article) private readonly articlesRepository: Repository<Article>,
    ) { }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    public async getAllArticles(title: string, skip: any): Promise<Article[]> {
        console.log(title, skip)
        const foundArticles: Article[] = await this.articlesRepository.find({
            where: {title: Like(`%${title  || ''}%`),
                    isDeleted: false},
            order: {timestamp: 'ASC'},
            take: 20,
            skip: skip || '0'
        });

        return foundArticles;
    }

    public async getArticleByID(articleID: string): Promise<Article> {

        const foundArticle: Article = await this.articlesRepository.findOne({
            where: {id: articleID, isDeleted: false}
        });

        if(!foundArticle) {
            throw new NotFoundException;
        }

        return foundArticle;
    }

    public async createArticle(article: CreateArticleDTO): Promise<Article> {
        const newArticle: Article = await this.articlesRepository.create();

        newArticle.title = article.title;
        newArticle.subTitle = article.subTitle;
        newArticle.url = article.url;
        newArticle.content = article.content;
        newArticle.author = article.author;
        newArticle.timestamp = new Date();

        const createdArticle = await this.articlesRepository.save(newArticle);

        console.log(JSON.stringify(article))

        return createdArticle;
    }

    public async updateArticle(articleID: string, article: Article): Promise<Article> {
        const foundArticle: Article = await this.articlesRepository.findOne({where: {id: articleID, isDeleted: false}});

        if (!foundArticle) {
            throw new NotFoundException;
        }

        foundArticle.content = article.content;
        foundArticle.title = article.title;
        foundArticle.subTitle = article.subTitle;
        foundArticle.url = article.url;
        
        await this.articlesRepository.update(articleID, foundArticle);
        
        return foundArticle;
    }

    public async deleteArticle(articleID: string): Promise<Article> {
        console.log(articleID)
        const foundArticle: Article = await this.articlesRepository.findOne({where: {id:articleID, isDeleted: false}});

        if (!foundArticle) {
            throw new NotFoundException;
        }

        await this.articlesRepository.update(articleID, {...foundArticle, isDeleted: true})

        return foundArticle;
    }
}
