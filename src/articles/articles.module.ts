import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { ConfigModule } from 'src/config/config.module';
import { Article } from 'src/data/entities/article.entity';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Article]),
        AuthModule,
        ConfigModule
    ],
    controllers: [ArticlesController],
    providers: [ArticlesService]
})
export class ArticlesModule {}
