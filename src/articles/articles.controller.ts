import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { Article } from '../data/entities/article.entity'
import { AuthGuard } from '@nestjs/passport';

@Controller('api/v1')
export class ArticlesController {
    public constructor(
        private readonly articlesService: ArticlesService,
    ) { }

    @Get('articles')
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    public async getAllArticles(@Query('title') title: string, @Query('skip') skip: number): Promise<Article[]> {
        return await this.articlesService.getAllArticles(title, skip);
    }

    @Get('articles/:articleID')
    public async getArtileByID(@Param('articleID') articleID: string): Promise<Article> {
        return await this.articlesService.getArticleByID(articleID);
    }

    @Post('articles')
    @UseGuards(AuthGuard())
    public async createArticle(
        @Body() article: Article
    ): Promise<Article> {
        return await this.articlesService.createArticle(article);
    }

    @Put('articles/edit/:articleID')
    @UseGuards(AuthGuard())
    public async updateArticle(
        @Param('articleID') articleID: string,
        @Body() article: Article
    ): Promise<Article> {
        return await this.articlesService.updateArticle(articleID, article);
    }

    @Delete('articles/delete/:articleID')
    @UseGuards(AuthGuard())
    public async deleteArticle(
        @Param('articleID') articleID: string
    ): Promise<Article> {
        return await this.articlesService.deleteArticle(articleID);
    }
}
