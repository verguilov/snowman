import { SNOWmanError } from './snowman-application.error';

export class InvalidUserCredentialsError extends SNOWmanError {
  constructor(message = 'Invalid username/email or password!') {
    super(message, 400);
  }
}